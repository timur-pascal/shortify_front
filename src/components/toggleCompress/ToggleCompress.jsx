import React from 'react';
import './ToggleCompress.css';

const ToggleCompress = ({ onClick=f=>f, isCompressed = false }) => {
    return (
        <button onClick={onClick}
                className={isCompressed ? 'button button_close' : 'button'}>
        </button>
    )
}

export default ToggleCompress;