import React from 'react';
import './Compressor.css'


const Compressor = ({
    link=null,
    isCompress=false,
    onChange=f=>f,
    onCopy=f=>f,
    hover=f=>f,
    leave=f=>f,
    onEnter=f=>f
     }) => {
    let input;
    const change = () => {
        onChange(input.value);
    }
    const checkKey = event => {
        if (event.keyCode === 13) onEnter();
    }

    return (
        <div className="compressor">
            <input  className="compressor__input compressor__input_raw"
                    type="text"
                    placeholder="Enter link"
                    ref={link => input = link}
                    onChange={change}
                    onKeyUp={checkKey}
                    autoFocus={true}
                    value={link}/>
            <input  className={isCompress ? 'compressor__input compressor__input_compress' : 'compressor__input compressor__input_compress compressor__input_disable'}
                    type="text"
                    value={link}
                    readOnly
                    onClick={onCopy}
                    onMouseEnter={hover}
                    onMouseLeave={leave}
                    tabIndex="-1"/>
        </div>
    )
}
export default Compressor;