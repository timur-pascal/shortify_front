import React, { Component } from 'react';
import './App.css';

import Compressor from './components/compressor/Compressor';
import ToggleCompress from './components/toggleCompress/ToggleCompress';
import MessageBox from './components/messageBox/MessageBox';
import Buffer from './components/buffer/buffer';
import compressLink from './service/api';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        originalLink: '',
        shortLink: '',
        message: '',
        oldLink: null,
        isCompress: false,
        isShowMessage: false,
        isBuffer: false
    }
  }

  toggleCompress(isCompress, linkToCompress) {
    if (isCompress) {
      this.setState({ isCompress: false })
    } else {
      if (linkToCompress !== this.state.oldLink) {
        this.getCompressedLink(linkToCompress);
      }
      else {
        this.setState({ isCompress: true });
      }
    }
  }

  getCompressedLink(rawLink) {
    if (rawLink !== '') {
      compressLink(rawLink).then(res => {
        if (res.status === 201) {
          this.setState({
            shortLink: res.data.shortLink,
            oldLink: rawLink,
            isCompress: true
          });
        } else {
          this.showMessage(res.data);
        }
      }).catch(error => {
        console.log(error);
      });
    }
  }

  hint(message) {
    const isShowMessage = true;
    this.setState({ message, isShowMessage });
  }

  showMessage(message) {
    this.hint(message);
    setTimeout(() => this.setState({ isShowMessage: false }), 2000);
  }

  copyLink(link) {
    navigator.clipboard.writeText(link);
    this.showMessage('Link is coped');
  }

  enterFromBuffer() {
    navigator.clipboard.readText().then(link => this.setState({ originalLink: link }));
  }

  bufferButton(isBuffer, isCompress) {
    return isBuffer && !isCompress ?
      <div className="buffer-wrapper">
        <Buffer onClick={() => this.enterFromBuffer()}/>
      </div> :
      ''
  }

  componentDidMount() {
    navigator.clipboard.readText().then(res => {
      if (res !== '') this.setState({ isBuffer: true });
    })
  }

  render() {
    const { originalLink, shortLink, isCompress, message, isShowMessage, isBuffer } = this.state;
    return (
      <div className="app">
        <main className="main">
            <h1 className="title">Shortify</h1>
            <div className="shortify">
                <Compressor link={isCompress ? shortLink : originalLink}
                            onChange={originalLink => this.setState({ originalLink })}
                            isCompress={isCompress}
                            onCopy={() => this.copyLink(shortLink)}
                            hover={() => this.hint('Click to copy')}
                            leave={() => this.setState({ isShowMessage: false })}
                            onEnter={() => this.getCompressedLink(originalLink)}/>
                <ToggleCompress
                            isCompressed={isCompress}
                            onClick={() => this.toggleCompress(isCompress, originalLink)}/>
                <MessageBox message={message}
                            isShowMessage={isShowMessage}
                            className="message-box"/>
                {this.bufferButton(isBuffer, isCompress)}
            </div>
        </main>
      </div>
    );
  }
}

export default App;
