import axios from 'axios';
import config from '../config/config';

const compressLink = async link => {
  return axios.post(`${config.api}/links`, {
      "link": link
    })
    .then(res => {
      return {
        status: res.status,
        data: res.data
      }
    })
    .catch(error => {
      return {
        status: error.response.status,
        data: error.response.data.message
      }
    });
}

export default compressLink;